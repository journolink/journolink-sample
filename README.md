# JournoLink Technical Test
Within this repository you will find a stock Laravel 5.6 application.

You can install this code into a [Laravel Homestead](https://laravel.com/docs/5.6/homestead) instance in order to launch it, alternatively you can use a local database instance in conjunction with the Laravel `php artisan serve` command.

## Scenario
We want to create a system to aggregate and analyse news stories in order to assess how they pertain to company-led press releases. This will involve collection of all news articles from a number of providers, storage, and analysis. 

You are tasked with prototyping a proof of concept to demonstrate this product's usefulness. The essential features are listed below:

 - Collection of news stories from one or more popular UK-based news agencies
 - A frontend which will display the news articles in a table for manual analysis
 
## Technical Test
Using the scenario above, you are to build a small, simple application with two facets; a data collection/retrieval system, and a front-end display system. Feel free to use whatever tools you have at your disposal to complete this Laravel project.
 
Below are a series of tips, do not feel that you have to be beholden to them, they are merely an aid. If you know of, and deem appropriate, alternative methods of satisfying the scenario, then go for it!
 
### Tips 
 - Creating an **artisan command** would be an appropriate way to collect the news articles, since this can be triggered manually from the command line, or used **within the Laravel scheduler**.
 - Most (if not all) UK news providers have a public RSS feed that is easy to parse. Example: https://metro.co.uk/feed/
 - **Consult the [Laravel documentation](https://laravel.com/docs/5.6)** - the documentation is extensive and covers all the various elements required in this test.
 - Design and display of the frontend is less important than appropriate use of routes and controllers to serve the page.
 - Storing and recalling data from the database must be done with **Laravel Models**.
 - Feel free to use composer to pull in any **third party libraries** you feel will help you satisfy the scenario.
 - Think about what OOP **design patterns and principles** you could bring in - such as [SOLID](https://scotch.io/bar-talk/s-o-l-i-d-the-first-five-principles-of-object-oriented-design), or at the very least an **appropriate level of abstraction**.
 - What **Laravel-specific Architecture Concepts** could you bring in (explanations of each are in the documentation)?
 
